<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname'=>'Ivan Ralph',
            'lastname'=>'Vitto',
            'email'=>'vittoivanralph@gmail.com',
            'birthdate'=>'1995-08-30',
            'user_type_id'=>'3',
            'status'=>'1',
            'location'=>'123',
            'password'=>bcrypt('test123')
        ]);
    }
}
