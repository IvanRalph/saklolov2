<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15/10/2018
 * Time: 11:21 AM
 */

return [
    'response' => [],

    'module_types' => [
        'text' => 1,
        'menu' => 2,
        'page' => 3,
        'api'  => 4
    ]
];