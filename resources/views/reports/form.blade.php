@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Incident Report</h1>
    <small>Choose your emergency</small>
@stop

@section('content')
<div class="row">
    <div class="col-md-4">
        <button type="button" class="btn btn-block btn-danger btn-lg"><h1><i class="fa fa-fire-extinguisher"></i></h1>
            Fire Fighter</button>
    </div>

    <div class="col-md-4">
        <button type="button" class="btn btn-block btn-warning btn-lg"><h1><i class="fa fa-ambulance"></i></h1>
            Medical Services</button>
    </div>

    <div class="col-md-4">
        <button type="button" class="btn btn-block btn-info btn-lg"><h1><i class="fa fa-heartbeat"></i></h1>
            Police Assistance</button>
    </div>
</div>
@stop