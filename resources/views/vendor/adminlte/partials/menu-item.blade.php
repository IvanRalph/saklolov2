<?php
$modules = \App\Module::join('user_type_accesses', 'modules.id', 'user_type_accesses.module_id')
    ->where('modules.status', 1)
    ->where('user_type_accesses.user_type_id', 3)
    ->where('showMenu', 1)
    ->get()->toArray();
?>

{{--TODO : target, icon_color, submenu--}}
@foreach ($modules as $module)
    @if($module['type'] == \App\Module::getTypes('menu'))
        <li {{ array_key_exists('class', $module) ? 'class='. $module['class'] .'' : '' }}>
            <a href="{{ $module['route'] }}"
               @if (isset($module['target'])) target="{{ $module['target'] }}" @endif
            >
                <i class="fa fa-fw fa-{{ isset($module['icon']) ? $module['icon'] : 'circle-o' }} {{ isset($module['icon_color']) ? 'text-' . $module['icon_color'] : '' }}"></i>
                <span>{{ $module['module_label'] }}</span>
                @if (isset($module['badge_label']))
                    <span class="pull-right-container">
                    <span class="label label-{{ isset($module['label_color']) ? $module['label_color'] : 'primary' }} pull-right">{{ $module['module_label'] }}</span>
                </span>
                @elseif (isset($module['submenu']))
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                @endif
            </a>
            @if (isset($module['submenu']))
                <ul class="{{ $module['submenu_class'] }}">
                    @include('adminlte::partials.menu-item')
                </ul>
            @endif
        </li>

    @else
        <li class="header">{{ $module['module_label'] }}</li>
    @endif
@endforeach