<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module_name', 50);
            $table->string('module_label', 50);
            $table->string('route', 100)->nullable();
            $table->string('icon', 40)->nullable();
            $table->string('class', 100)->nullable();
            $table->smallInteger('type');
            $table->string('badge_label', 10)->nullable();
            $table->string('label_color', 20)->nullable();
            $table->string('controller', 50)->nullable();
            $table->string('function', 50)->nullable();
            $table->string('method', '20')->nullable();
            $table->string('route_type', 10)->nullable();
            $table->string('auth')->nullable();
            $table->smallInteger('status')->default(1);
            $table->smallInteger('showMenu')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
