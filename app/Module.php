<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'modules';

    public function userTypeAccesses ()
    {
        return $this->hasMany(UserTypeAccess::class);
    }

    public static function getTypes ($type)
    {
        return config()->get('constants.module_types.' . $type);
    }
}
