<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTypeAccessesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_type_accesses')->insert([
            [
                'module_id'=>1,
                'user_type_id'=>3
            ],[
                'module_id'=>2,
                'user_type_id'=>3
            ],[
                'module_id'=>3,
                'user_type_id'=>3
            ],
            [
                'module_id'=>4,
                'user_type_id'=>3
            ],
            [
                'module_id'=>5,
                'user_type_id'=>3
            ]
        ]);
    }
}
