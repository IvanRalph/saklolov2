<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$modules = \App\Module::join('user_type_accesses', 'modules.id', 'user_type_accesses.module_id')
    ->where('modules.status', 1)
    ->where('user_type_accesses.user_type_id', 3)
    ->get();

Route::group(['middleware' => ['web', 'revalidate'], 'prefix' => ''], function () use ($modules) {
    foreach ($modules as $module) {
        if ($module->function && $module->route_type == 'web') {
            $method = $module->method;
            Route::$method($module->route, $module->controller . '@' . $module->function)->middleware($module->auth == 1 ? 'auth' : 'guest');
        }
    }
});

Route::group(['middleware' => ['api', 'revalidate'], 'prefix' => ''], function () use ($modules) {
    foreach ($modules as $module) {
        if ($module->function && $module->route_type == 'api') {
            $method = $module->method;
            Route::$method($module->route, $module->controller . '@' . $module->function)->middleware($module->auth == 1 ? 'auth' : 'guest');
        }
    }
});

Route::get('/', 'HomeController@index');