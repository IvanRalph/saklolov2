<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create('report_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reporter_id')->unsigned();
            $table->integer('dept_id')->unsigned();
            $table->longText('location');
            $table->integer('status');
            $table->integer('authority_id')->unsigned();
            $table->longText('notes')->nullable();
            $table->string('image', 50)->nullable();
            $table->string('audio', 50)->nullable();
            $table->dateTime('attended_at')->nullable();
            $table->timestamps();

            $table->foreign('reporter_id')
                ->references('id')->on('users')
                ->onUpdate('cascade');

            $table->foreign('dept_id')
                ->references('id')->on('departments')
                ->onUpdate('cascade');

            $table->foreign('authority_id')
                ->references('id')->on('users')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists('report_details');
    }
}
