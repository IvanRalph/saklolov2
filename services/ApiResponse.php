<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15/10/2018
 * Time: 11:30 AM
 */

namespace services;
use services\interfaces\ResponseInterface;

class ApiResponse implements ResponseInterface
{
    public function render($template){
        return view($template)->render();
    }
}