<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Module;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table('modules')->insert([
            [
                'module_name'  => 'login_form',
                'module_label' => 'Login',
                'route'        => 'login',
                'icon'         => null,
                'type'         => Module::getTypes('page'),
                'controller'   => 'LoginController',
                'function'     => 'index',
                'showMenu'     => 0,
                'method'       => 'get',
                'route_type'   => 'web',
                'auth'         => 0
            ],
            [
                'module_name'  => 'login_authenticate',
                'module_label' => 'Login Post',
                'route'        => 'login',
                'icon'         => null,
                'type'         => Module::getTypes('api'),
                'controller'   => 'LoginController',
                'function'     => 'store',
                'showMenu'     => 0,
                'method'       => 'post',
                'route_type'   => 'api',
                'auth'         => 0
            ],
            [
                'module_name'  => 'logout',
                'module_label' => 'Logout',
                'route'        => 'logout',
                'icon'         => null,
                'type'         => Module::getTypes('api'),
                'controller'   => 'LoginController',
                'function'     => 'logout',
                'showMenu'     => 0,
                'method'       => 'post',
                'route_type'   => 'api',
                'auth'         => 1
            ],
            [
                'module_name'  => 'administrators',
                'module_label' => 'ADMINISTRATORS',
                'route'        => null,
                'icon'         => null,
                'type'         => Module::getTypes('text'),
                'controller'   => null,
                'function'     => null,
                'showMenu'     => 1,
                'method'       => null,
                'route_type'   => null,
                'auth'         => null
            ],
            [
                'module_name'  => 'system_maintenance',
                'module_label' => 'System Maintenance',
                'route'        => 'modules',
                'icon'         => 'cog',
                'type'         => Module::getTypes('menu'),
                'controller'   => 'ModuleController',
                'function'     => 'index',
                'showMenu'     => 1,
                'method'       => 'get',
                'route_type'   => 'web',
                'auth'         => 1
            ]
        ]);
    }
}
