<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTypeAccess extends Model
{
    protected $table = 'user_type_accesses';

    public function modules ()
    {
        return $this->belongsTo(Module::class, 'id');
    }
}
