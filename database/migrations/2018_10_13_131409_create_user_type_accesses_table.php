<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypeAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_type_accesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('module_id', false, true);
            $table->integer('user_type_id', false, true);
            $table->timestamps();

            $table->foreign('module_id')
                ->references('id')->on('modules')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('user_type_id')
                ->references('id')->on('user_types')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_type_accesses');
    }
}
