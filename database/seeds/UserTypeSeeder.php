<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->insert([
            [
                'description'=>'User',
                'status'=>1,
            ],
            [
                'description'=>'Authority',
                'status'=>1,
            ],
            [
                'description'=>'Admin',
                'status'=>1,
            ]
        ]);
    }
}
