<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 15/10/2018
 * Time: 12:19 PM
 */

namespace services\interfaces;

interface ResponseInterface
{
    public function render($template);
}